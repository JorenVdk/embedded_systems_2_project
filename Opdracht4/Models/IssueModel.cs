﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Opdracht4.Models
{
    public class IssueModel
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonRequired]
        public string Title { get; set; }
        public string Assignee { get; set; }
        public string Message { get; set; }
        public bool Completed { get; set; }
        public DateTime? TimeMade { get; set; } = DateTime.Now;
    }
}
