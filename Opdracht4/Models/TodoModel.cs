﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Opdracht4.Models
{
    public class TodoModel
    {
        [BsonId]
        public ObjectId TodoId { get; set; }
        [BsonRequired]
        public string Title { get; set; }
        public bool Completed { get; set; }
        public DateTime? OptimalLine { get; set; } = null;
    }
}
