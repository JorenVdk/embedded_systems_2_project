﻿$(document).ready(function () { 
    // Load the Visualization API and the corechart package.
    google.charts.load('current', { 'packages': ['gauge'] });
    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChart);
    var temperature = 0;
    var chart; 
    var data;
    var options;
    function drawChart() {
        data = google.visualization.arrayToDataTable([
            ['Label', 'Value'],
            ['temperature', 20]
        ]);

        options = {
            width: 800, height: 240,
            redFrom: 30, redTo: 40,
            greenFrom: 20, greenTo: 30,
            minorTicks: 5,
            max: 40,
            min: -10
        };

        chart = new google.visualization.Gauge(document.getElementById('chart_div'));

        data.setValue(0, 1, temperature);
        chart.draw(data, options);
    }  

    function PollStatus() {
        $.get("/Home/GetTemperature", function (data) {
            temperature = data;        
            data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['temperature', 20]
            ]);
            data.setValue(0, 1, temperature);
            chart.draw(data, options);
        });        
    }          
    setInterval(PollStatus, 3000);
});