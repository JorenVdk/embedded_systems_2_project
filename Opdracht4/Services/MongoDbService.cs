﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Opdracht4.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Opdracht4.Services
{
    public class MongoDbService
    {
        private IMongoCollection<IssueModel> IssueCollection { get; }
        public MongoDbService(string databaseName, string collectionName, string databaseUrl)
        {
            var mongoClient = new MongoClient(databaseUrl);
            var mongoDatabase = mongoClient.GetDatabase(databaseName);

            IssueCollection = mongoDatabase.GetCollection<IssueModel>(collectionName);
        }  

        public async Task InsertIssue(IssueModel issue) => await IssueCollection.InsertOneAsync(issue);
        public async Task<List<IssueModel>> GetAllIssues()
        {
            var issues = new List<IssueModel>();
            var allDocuments = await IssueCollection.FindAsync(new BsonDocument());
            await allDocuments.ForEachAsync(doc => issues.Add(doc));

            return issues;
        }   
        public async Task<IssueModel> GetIssueById(string id)
        {
            var pid = ObjectId.Parse(id);
            var issue = new IssueModel();
            var issues = new List<IssueModel>();

            var allDocuments = await IssueCollection.FindAsync(new BsonDocument());
            await allDocuments.ForEachAsync(doc => issues.Add(doc));
            issue = issues.Where(i => i.Id == pid).FirstOrDefault();

            return issue;
        }
        public async Task ReplaceIssue(IssuePostModel issuePost)
        {
            var issue = new IssueModel
            {
                Id = ObjectId.Parse(issuePost.Id),
                Title = issuePost.Title,
                Assignee = issuePost.Assignee,
                Message = issuePost.Message,
                Completed = issuePost.Completed
            };
            var filter = Builders<IssueModel>.Filter.Eq(s => s.Id, issue.Id);
            var result = await IssueCollection.ReplaceOneAsync(filter, issue);
        }
        public async Task IssueToDelete(string id)
        {
            var pid = ObjectId.Parse(id);
            await IssueCollection.DeleteOneAsync(i => i.Id == pid);
        }
    }
}
