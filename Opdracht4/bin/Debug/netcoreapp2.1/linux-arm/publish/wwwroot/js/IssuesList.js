﻿$(document).ready(function () {
    var ID;
    $(".fa-edit").click(function () {
        var pid = $(this).parent().parent().data('pid');
        $.post("/Home/GetIssueToEdit", { "pid": pid }, function (data) {
            var url = data;
            window.location.href = url;
        }); 
    });

    $(".fa-trash-alt").click(function () {
        ID = $(this).parent().parent().data('pid');
        $('#deleteModal').modal('show');
    });

    $("#yesButton").click(function () {
        $.post("/Home/IssueToDelete", { "id": ID }, function (data) {
            $('#deleteModal').modal('hide');
            var url = data;
            window.location.href = url;
        });
    });
});