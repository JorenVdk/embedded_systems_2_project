﻿"use strict";
var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();
var Message;
Message = function (arg) {
    this.text = arg.text, this.user = arg.user, this.message_side = arg.message_side;
    this.draw = function (_this) {
        return function () {
            var $message;
            $message = $($('.message_template').clone().html());
            $message.addClass(_this.message_side).find('.text').html(_this.text);
            $message.addClass(_this.message_side).find('.user').html(_this.user);
            $('.messages').append($message);
            return setTimeout(function () {
                return $message.addClass('appeared');
            }, 0);
        };
    }(this);
    return this;
};
$(document).ready(function () {    
    connection.on("ReceiveMessage", function (user, message) {
        var $messages, messageReceived;         
        var text = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        var message_side = "left";
        $messages = $('.messages');
        messageReceived = new Message({
            text: text,
            user: user,
            message_side: message_side
        });
        messageReceived.draw();
        return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
    });

    connection.start().catch(function (err) {
        return console.error(err.toString());
    });
        
    var getMessageText, message_side, sendMessage, getUser;
    message_side = 'right';
    getMessageText = function () {
        var $message_input;
        $message_input = $('.message_input');
        return $message_input.val();
    };
    getUser = function () {
        var $userName_input;
        $userName_input = $('.userName_input');
        return $userName_input.val();
    };
    sendMessage = function (text, user) {
        var $messages, message;
        if (text.trim() === '') {
            return;
        }
        $('.message_input').val('');
        $messages = $('.messages');                                  
        message = new Message({
            text: text,
            user: user,
            message_side: message_side
        });
        message.draw();
        SendMessageToOthers(user, text);
        return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
    };
    $('.send_message').click(function (e) {
        return sendMessage(getMessageText(), getUser());
    });
    $('.message_input').keyup(function (e) {
        if (e.which === 13) {
            return sendMessage(getMessageText(), getUser());
        }
    });  
});

function SendMessageToOthers(user, message) {
    connection.invoke("SendMessage", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
}