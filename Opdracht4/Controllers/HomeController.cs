﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Opdracht4.Models;
using Opdracht4.Services;
using Unosquare.RaspberryIO;

namespace Opdracht4.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CreateIssue()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateIssue(IssueModel issue)
        {   
            var mongoDbService = new MongoDbService("IssueDatabase", "Issues", "mongodb://raspv");

            await mongoDbService.InsertIssue(issue);

            return RedirectToAction("CreateIssue");
        }
        
        public async Task<IActionResult> IssuesList()
        {
            var mongoDbService = new MongoDbService("IssueDatabase", "Issues", "mongodb://raspv");                    
            var Model = await mongoDbService.GetAllIssues();
            return View(Model);
        }

        [HttpGet]
        [Route("Home/GetTemperature")]
        public IActionResult GetTemperature()
        {
            // Register a device on the bus
            var myDevice = Pi.I2C.AddDevice(0x48);
           
            var response = myDevice.Read().ToString();                
            int decValue = Convert.ToInt32(response);

            return Json(decValue);
        }

        [HttpPost]
        [Route("Home/GetIssueToEdit")]
        public IActionResult GetIssueToEdit(string pid)
        {
            
            return Json(Url.Action("EditIssue", "Home", new { id = pid }));
        }

        [HttpGet]
        [Route("Home/EditIssue")]
        public async Task<IActionResult> EditIssue(string id)
        {
            var mongoDbService = new MongoDbService("IssueDatabase", "Issues", "mongodb://raspv");      
            var model = await mongoDbService.GetIssueById(id);
            return View(model);
        }

        [HttpPost]
        [Route("Home/EditIssue")]
        public async Task<IActionResult> EditIssue(IssuePostModel issuePost)
        {
            var mongoDbService = new MongoDbService("IssueDatabase", "Issues", "mongodb://raspv");     
            await mongoDbService.ReplaceIssue(issuePost);
            return RedirectToAction("IssuesList");
        }

        [HttpPost]
        [Route("Home/IssueToDelete")]
        public async Task<IActionResult> IssueToDelete(string id)
        {
            var mongoDbService = new MongoDbService("IssueDatabase", "Issues", "mongodb://raspv");      
            await mongoDbService.IssueToDelete(id);
            return Json(Url.Action("IssuesList"));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
